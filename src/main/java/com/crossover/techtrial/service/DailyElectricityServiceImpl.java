package com.crossover.techtrial.service;

import com.crossover.techtrial.dto.DailyElectricityDTO;
import com.crossover.techtrial.model.DailyElectricity;
import com.crossover.techtrial.model.Panel;
import com.crossover.techtrial.repository.DailyElectricityRepository;
import com.crossover.techtrial.service.exceptions.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

@Service
public class DailyElectricityServiceImpl implements DailyElectricityService {

    private DailyElectricityRepository dailyElectricityRepository;
    private PanelService panelService;

    public DailyElectricityServiceImpl(DailyElectricityRepository dailyElectricityRepository, PanelService panelService) {
        this.dailyElectricityRepository = dailyElectricityRepository;
        this.panelService = panelService;
    }

    @Override
    public DailyElectricity save(DailyElectricity dailyElectricity) {
        return this.dailyElectricityRepository.save(dailyElectricity);
    }

    @Override
    public List<DailyElectricityDTO> getAllDailyElectricityByPanelId(String panelSerial) {
        Panel panel = this.panelService.getBySerial(panelSerial);
        final List<DailyElectricity> entities = this.dailyElectricityRepository.findAllByPanelId(panel.getId());

        if (entities.isEmpty()) {
            throw new ResourceNotFoundException(format("No daily electricity report found for the panel '%s'", panelSerial));
        }

        return entities.stream()
                .map(DailyElectricityDTO::new)
                .collect(toList());
    }
}
