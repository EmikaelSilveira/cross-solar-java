package com.crossover.techtrial.model;

public enum InitialProcessStatus {
    STARTED,
    FINISHED,
    FAILED
}
