package com.crossover.techtrial.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "initial_process")
public class InitialProcess implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private InitialProcessStatus status;

    public InitialProcess() {
        super();
    }

    public InitialProcess(Long id, InitialProcessStatus status) {
        this.id = id;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public InitialProcessStatus getStatus() {
        return status;
    }

    public void setStatus(InitialProcessStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "InitialProcess{" +
                "id=" + id +
                ", status=" + status +
                '}';
    }
}
