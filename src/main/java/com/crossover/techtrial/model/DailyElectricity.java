package com.crossover.techtrial.model;

import com.crossover.techtrial.dto.DailyElectricityDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "daily_electricity")
public class DailyElectricity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "panel_id", referencedColumnName = "id")
    private Panel panel;

    @Column(name = "generated_electricity_sum")
    private Long generatedElectricitySum;

    @Column(name = "generated_electricity_min")
    private Long generatedElectricityMin;

    @Column(name = "generated_electricity_max")
    private Long generatedElectricityMax;

    @Column(name = "generated_electricity_average")
    private Double generatedElectricityAverage;

    @Column(name = "reading_at")
    private LocalDateTime readingAt;

    public DailyElectricity() {
        super();
    }

    public DailyElectricity(Panel panel, Long generatedElectricitySum, Long generatedElectricityMin,
                            Long generatedElectricityMax, Double generatedElectricityAverage, LocalDateTime readingAt) {
        this.panel = panel;
        this.generatedElectricitySum = generatedElectricitySum;
        this.generatedElectricityMin = generatedElectricityMin;
        this.generatedElectricityMax = generatedElectricityMax;
        this.generatedElectricityAverage = generatedElectricityAverage;
        this.readingAt = readingAt;
    }

    public DailyElectricity(DailyElectricityDTO dto) {
        this.panel = new Panel(dto.getPanelId());
        this.generatedElectricityAverage = dto.getAverage().doubleValue();
        this.generatedElectricityMax = dto.getMax();
        this.generatedElectricityMin = dto.getMin();
        this.generatedElectricitySum = dto.getSum();
        this.readingAt = dto.getDate().atStartOfDay();
    }

    public Long getId() {
        return id;
    }

    public Panel getPanel() {
        return panel;
    }

    public void setPanel(Panel panel) {
        this.panel = panel;
    }

    public Long getGeneratedElectricitySum() {
        return generatedElectricitySum;
    }

    public Long getGeneratedElectricityMin() {
        return generatedElectricityMin;
    }

    public Long getGeneratedElectricityMax() {
        return generatedElectricityMax;
    }

    public Double getGeneratedElectricityAverage() {
        return generatedElectricityAverage;
    }

    public LocalDateTime getReadingAt() {
        return readingAt;
    }

    @Override
    public String toString() {
        return "DailyElectricityDTO{" + "id=" + id
                + ", panel=" + panel
                + ", generatedElectricitySum=" + generatedElectricitySum
                + ", generatedElectricityMin=" + generatedElectricityMin
                + ", generatedElectricityMax=" + generatedElectricityMax
                + ", generatedElectricityAverage=" + generatedElectricityAverage
                + ", readingAt=" + readingAt + '}';
    }
}
