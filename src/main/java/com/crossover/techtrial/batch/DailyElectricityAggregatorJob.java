package com.crossover.techtrial.batch;

import com.crossover.techtrial.model.DailyElectricity;
import com.crossover.techtrial.model.HourlyElectricity;
import com.crossover.techtrial.model.Panel;
import com.crossover.techtrial.service.DailyElectricityService;
import com.crossover.techtrial.service.HourlyElectricityService;
import com.crossover.techtrial.service.PanelService;
import net.javacrumbs.shedlock.core.SchedulerLock;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Objects;

@Component
@ConditionalOnProperty(value = "crosssolar.scheduler.enabled", havingValue = "true", matchIfMissing = true)
public class DailyElectricityAggregatorJob {

    protected static final String ZONE_ID = "CST6CDT";
    private static final String CRON = "0 0 0 * * *";

    private HourlyElectricityService hourlyElectricityService;
    private DailyElectricityService dailyElectricityService;
    private PanelService panelService;

    public DailyElectricityAggregatorJob(HourlyElectricityService hourlyElectricityService, DailyElectricityService dailyElectricityService, PanelService panelService) {
        this.hourlyElectricityService = hourlyElectricityService;
        this.dailyElectricityService = dailyElectricityService;
        this.panelService = panelService;
    }

    @Scheduled(cron = CRON, zone = ZONE_ID)
    @SchedulerLock(name = "consolidateDailyElectricityGeneration")
    public void consolidateDailyElectricityGeneration() {
        process();
    }

    private void process() {
        this.panelService.getAll().forEach(panel -> {
            final LongSummaryStatistics summaryStatistics = getAllHourlyElectricity(panel)
                    .stream()
                    .filter(this::isNotEmpty)
                    .mapToLong(HourlyElectricity::getGeneratedElectricity)
                    .summaryStatistics();

            this.dailyElectricityService.save(getDailyElectricity(panel, summaryStatistics));
        });
    }

    private DailyElectricity getDailyElectricity(Panel panel, LongSummaryStatistics summaryStatistics) {
        return new DailyElectricity(panel, summaryStatistics.getSum(),
                summaryStatistics.getMin(), summaryStatistics.getMax(), summaryStatistics.getAverage(),
                yesterdayAtStartOfDay());
    }

    private boolean isNotEmpty(HourlyElectricity hourlyElectricity) {
        return Objects.nonNull(hourlyElectricity);
    }

    private List<HourlyElectricity> getAllHourlyElectricity(Panel panel) {
        return this.hourlyElectricityService.getAllHourlyElectricityByReadingAtBetween(
                yesterdayAtStartOfDay(), yesterdayAtEndOfDay(), panel);
    }

    private LocalDateTime yesterdayAtStartOfDay() {
        return LocalDate.now(ZoneId.of(ZONE_ID)).minusDays(1).atStartOfDay();
    }

    private LocalDateTime yesterdayAtEndOfDay() {
        LocalDate yesterday = LocalDate.now(ZoneId.of(ZONE_ID)).minusDays(1);
        return LocalDateTime.of(yesterday, LocalTime.MAX);
    }

}
