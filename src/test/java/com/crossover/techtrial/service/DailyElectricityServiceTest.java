package com.crossover.techtrial.service;

import com.crossover.techtrial.dto.DailyElectricityDTO;
import com.crossover.techtrial.model.DailyElectricity;
import com.crossover.techtrial.repository.DailyElectricityRepository;
import com.crossover.techtrial.service.exceptions.ResourceNotFoundException;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import static com.crossover.techtrial.utils.TestUtils.dailyElectricity;
import static com.crossover.techtrial.utils.TestUtils.panel;
import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DailyElectricityServiceTest {

    private DailyElectricityService dailyElectricityService;
    private DailyElectricityRepository dailyElectricityRepository;

    @Before
    public void setUp() {
        final PanelService panelService = mock(PanelService.class);
        when(panelService.getBySerial(any())).thenReturn(panel());

        this.dailyElectricityRepository = mock(DailyElectricityRepository.class);
        this.dailyElectricityService = new DailyElectricityServiceImpl(this.dailyElectricityRepository, panelService);
    }

    @Test
    public void save() {
        final DailyElectricity expected = dailyElectricity();
        when(this.dailyElectricityRepository.save(any())).thenReturn(expected);

        final DailyElectricity result = this.dailyElectricityService.save(expected);
        assertEquals(expected, result);
    }

    @Test
    public void getAllDailyElectricityByPanelId() {
        final DailyElectricity dailyElectricity = dailyElectricity();
        final List<DailyElectricity> expected = Collections.singletonList(dailyElectricity);
        when(this.dailyElectricityRepository.findAllByPanelId(any())).thenReturn(expected);

        final List<DailyElectricityDTO> result = this.dailyElectricityService.getAllDailyElectricityByPanelId("random-serial");
        result.forEach(validate(dailyElectricity));
    }

    private Consumer<DailyElectricityDTO> validate(final DailyElectricity dailyElectricity) {
        return dto -> validateDto(dailyElectricity, dto);
    }

    private void validateDto(DailyElectricity dailyElectricity, DailyElectricityDTO dto) {
        assertEquals(dailyElectricity.getGeneratedElectricityAverage().longValue(), dto.getAverage().longValue());
        assertEquals(dailyElectricity.getGeneratedElectricityMax(), dto.getMax());
        assertEquals(dailyElectricity.getGeneratedElectricityMin(), dto.getMin());
        assertEquals(dailyElectricity.getGeneratedElectricitySum(), dto.getSum());
        assertEquals(dailyElectricity.getReadingAt().toLocalDate(), dto.getDate());
        assertEquals(dailyElectricity.getPanel().getId(), dto.getPanelId());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void getAllDailyElectricityByPanelId_notFound() {
        when(this.dailyElectricityRepository.findAllByPanelId(any())).thenReturn(emptyList());
        this.dailyElectricityService.getAllDailyElectricityByPanelId("random-serial");
    }

}